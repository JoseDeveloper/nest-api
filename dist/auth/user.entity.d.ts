export declare class User {
    id: string;
    username: string;
    email: string;
    password: string;
    active: boolean;
    roleId: number;
    createdOn: Date;
    updatedOn: Date;
}
