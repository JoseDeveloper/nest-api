export declare class RegisterUserDto {
    username: string;
    email: string;
    password: string;
    roleId: number;
}
