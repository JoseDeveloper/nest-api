"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const typeorm_1 = require("@nestjs/typeorm");
const encoder_service_1 = require("./encoder.service");
const users_repository_1 = require("./users.repository");
let AuthService = class AuthService {
    constructor(usersRepository, encoderService, jwtService) {
        this.usersRepository = usersRepository;
        this.encoderService = encoderService;
        this.jwtService = jwtService;
    }
    async registerUser(registerUserDto) {
        const { username, email, password, roleId } = registerUserDto;
        const hashedPassword = await this.encoderService.encodePassword(password);
        return this.usersRepository.createUser(username, email, hashedPassword, roleId);
    }
    async login(loginDto) {
        const { email, password } = loginDto;
        const user = await this.usersRepository.findOneByEmail(email);
        if (user && (await this.encoderService.checkPassword(password, user.password))) {
            const payload = { id: user.id, email, active: user.active };
            const accessToken = await this.jwtService.sign(payload);
            const { username, roleId, id } = user;
            return { access_token: accessToken, userData: { username, role_id: roleId, id } };
        }
        throw new common_1.UnauthorizedException('Please check your credentials');
    }
    async getUser(userPayloadDto) {
        return this.usersRepository.findByPayload(userPayloadDto);
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(users_repository_1.UsersRepository)),
    __metadata("design:paramtypes", [users_repository_1.UsersRepository,
        encoder_service_1.EncoderService,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map