import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { UserPayloadDto } from './dto/user-payload.dto';
import { RegisterUserDto } from './dto/register-user.dto';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    register(registerUserDto: RegisterUserDto): Promise<void>;
    login(loginDto: LoginDto): Promise<Object>;
    getUser(userPayloadDto: UserPayloadDto): Promise<Object>;
}
