import { Repository } from 'typeorm';
import { User } from './user.entity';
export declare class UsersRepository extends Repository<User> {
    createUser(username: string, email: string, password: string, roleId: number): Promise<void>;
    findOneByEmail(email: string): Promise<User>;
    findByPayload({ name }: any): Promise<User>;
}
