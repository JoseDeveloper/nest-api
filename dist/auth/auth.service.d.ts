import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { UserPayloadDto } from './dto/user-payload.dto';
import { EncoderService } from './encoder.service';
import { User } from './user.entity';
import { UsersRepository } from './users.repository';
export declare class AuthService {
    private usersRepository;
    private encoderService;
    private jwtService;
    constructor(usersRepository: UsersRepository, encoderService: EncoderService, jwtService: JwtService);
    registerUser(registerUserDto: RegisterUserDto): Promise<void>;
    login(loginDto: LoginDto): Promise<Object>;
    getUser(userPayloadDto: UserPayloadDto): Promise<User>;
}
