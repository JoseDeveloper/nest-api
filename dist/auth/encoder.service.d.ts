export declare class EncoderService {
    encodePassword(password: string): Promise<string>;
    checkPassword(password: string, userPassword: string): Promise<boolean>;
}
