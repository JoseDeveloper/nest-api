import { ReservationDto } from './dto/reservation.dto';
import { ReservationService } from './reservation.service';
import { Reservation } from './reservation.entity';
export declare class ReservationController {
    private reservationService;
    constructor(reservationService: ReservationService);
    getAll(): Promise<Object[]>;
    getByUserId(userId: string): Promise<Object[]>;
    register(reservationDto: ReservationDto): Promise<Reservation[]>;
    update(id: number, reservationDto: Reservation): Promise<Reservation[]>;
    delete(id: number): Promise<Reservation[]>;
}
