import { Repository } from 'typeorm';
import { Reservation } from './reservation.entity';
export declare class ReservationRepository extends Repository<Reservation> {
    createReserve(userId: string, title: string, reservationDate: Date, start: Date, end: Date, reserve: string, spaceType: string, eventType: string): Promise<Reservation>;
    updateReserve(reservationDto: Reservation): Promise<Reservation>;
    findAll(): Promise<Reservation[]>;
    findByUserId(userId: any): Promise<Reservation[]>;
}
