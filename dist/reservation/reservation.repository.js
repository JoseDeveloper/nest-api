"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const reservation_entity_1 = require("./reservation.entity");
let ReservationRepository = class ReservationRepository extends typeorm_1.Repository {
    async createReserve(userId, title, reservationDate, start, end, reserve, spaceType, eventType) {
        const reservation = this.create({ userId, title, reservationDate, start, end, reserve, spaceType, eventType });
        try {
            return await this.save(reservation);
        }
        catch (e) {
            console.log(e);
            if (e.code === 'ER_DUP_ENTRY') {
                throw new common_1.ConflictException('Error saving reservation');
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async updateReserve(reservationDto) {
        const reservation = this.merge(reservationDto);
        try {
            return await this.save(reservation);
        }
        catch (e) {
            console.log(e);
            if (e.code === 'ER_DUP_ENTRY') {
                throw new common_1.ConflictException('Error saving reservation');
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async findAll() {
        return await this.find();
    }
    async findByUserId(userId) {
        return await this.find({ userId });
    }
};
ReservationRepository = __decorate([
    (0, typeorm_1.EntityRepository)(reservation_entity_1.Reservation)
], ReservationRepository);
exports.ReservationRepository = ReservationRepository;
//# sourceMappingURL=reservation.repository.js.map