import { User } from 'src/auth/user.entity';
export declare class Reservation {
    id: string;
    userId: string;
    user: User;
    title: string;
    reservationDate: Date;
    start: Date;
    end: Date;
    reserve: string;
    spaceType: string;
    eventType: string;
    createdOn: Date;
    updatedOn: Date;
}
