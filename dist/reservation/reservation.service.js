"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const reservation_repository_1 = require("./reservation.repository");
let ReservationService = class ReservationService {
    constructor(reservationRepository) {
        this.reservationRepository = reservationRepository;
    }
    async getAll() {
        return this.reservationRepository.find({ relations: ['user'] });
    }
    async getByUserId(userId) {
        return this.reservationRepository.findByUserId(userId);
    }
    async registerReservation(reservationDto) {
        const { userId, title, reservationDate, start, end, reserve, spaceType, eventType } = reservationDto;
        await this.reservationRepository.createReserve(userId, title, reservationDate, start, end, reserve, spaceType, eventType);
        return await this.reservationRepository.findByUserId(userId);
    }
    async updateReservation(id, reservationDto) {
        const { userId } = reservationDto;
        await this.reservationRepository.updateReserve(reservationDto);
        return await this.reservationRepository.findByUserId(userId);
    }
    async delete(id) {
        await this.reservationRepository.delete(id);
        return this.reservationRepository.find({ relations: ['user'] });
    }
};
ReservationService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(reservation_repository_1.ReservationRepository)),
    __metadata("design:paramtypes", [reservation_repository_1.ReservationRepository])
], ReservationService);
exports.ReservationService = ReservationService;
//# sourceMappingURL=reservation.service.js.map