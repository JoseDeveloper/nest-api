export declare class ReservationDto {
    id: string;
    userId: string;
    title: string;
    reservationDate: Date;
    start: Date;
    end: Date;
    reserve: string;
    spaceType: string;
    eventType: string;
    createdOn: Date;
    updatedOn: Date;
}
