import { ReservationRepository } from './reservation.repository';
import { ReservationDto } from './dto/reservation.dto';
import { Reservation } from './reservation.entity';
export declare class ReservationService {
    private reservationRepository;
    constructor(reservationRepository: ReservationRepository);
    getAll(): Promise<Object[]>;
    getByUserId(userId: string): Promise<Object[]>;
    registerReservation(reservationDto: ReservationDto): Promise<Reservation[]>;
    updateReservation(id: any, reservationDto: Reservation): Promise<Reservation[]>;
    delete(id: any): Promise<Reservation[]>;
}
