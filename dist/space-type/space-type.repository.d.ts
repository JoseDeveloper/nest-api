import { Repository } from 'typeorm';
import { SpaceType } from './space-type.entity';
export declare class SpaceTypeRepository extends Repository<SpaceType> {
    findAll(): Promise<SpaceType[]>;
}
