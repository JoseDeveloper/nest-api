import { SpaceTypeService } from './space-type.service';
export declare class SpaceTypeController {
    private spaceTypeService;
    constructor(spaceTypeService: SpaceTypeService);
    getAll(): Promise<Object[]>;
}
