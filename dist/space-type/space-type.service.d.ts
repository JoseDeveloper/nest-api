import { SpaceTypeRepository } from './space-type.repository';
export declare class SpaceTypeService {
    private spaceTypeRepository;
    constructor(spaceTypeRepository: SpaceTypeRepository);
    getAll(): Promise<Object[]>;
}
