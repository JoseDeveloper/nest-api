"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpaceTypeRepository = void 0;
const typeorm_1 = require("typeorm");
const space_type_entity_1 = require("./space-type.entity");
let SpaceTypeRepository = class SpaceTypeRepository extends typeorm_1.Repository {
    async findAll() {
        return await this.findAll();
    }
};
SpaceTypeRepository = __decorate([
    (0, typeorm_1.EntityRepository)(space_type_entity_1.SpaceType)
], SpaceTypeRepository);
exports.SpaceTypeRepository = SpaceTypeRepository;
//# sourceMappingURL=space-type.repository.js.map