"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpaceTypeModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const space_type_controller_1 = require("./space-type.controller");
const space_type_service_1 = require("./space-type.service");
const space_type_repository_1 = require("./space-type.repository");
let SpaceTypeModule = class SpaceTypeModule {
};
SpaceTypeModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([space_type_repository_1.SpaceTypeRepository])],
        controllers: [space_type_controller_1.SpaceTypeController],
        providers: [space_type_service_1.SpaceTypeService]
    })
], SpaceTypeModule);
exports.SpaceTypeModule = SpaceTypeModule;
//# sourceMappingURL=space-type.module.js.map