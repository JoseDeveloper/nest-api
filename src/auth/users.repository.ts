import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { User } from './user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {

  async createUser(username: string, email: string, password: string, roleId: number): Promise<void> {
    const user = this.create({ username, email, password, roleId });

    try {
      await this.save(user);
    } catch (e) {
      if (e.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Error in registration');
      }
      throw new InternalServerErrorException();
    }
  }

  async findOneByEmail(email: string): Promise<User> {
    return await this.findOne({ email });
  }

  async findByPayload({ name }: any): Promise<User> {
    return await this.findOne({ 
        where:  { name } });  
  }
}
