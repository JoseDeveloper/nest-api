import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { LoginDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { UserPayloadDto } from './dto/user-payload.dto'; 
import { EncoderService } from './encoder.service';
import { JwtPayload } from './jwt-payload.interface';
import { User } from './user.entity';
import { UsersRepository } from './users.repository';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UsersRepository)
    private usersRepository: UsersRepository,
    private encoderService: EncoderService,
    private jwtService: JwtService,
  ) {}

  async registerUser(registerUserDto: RegisterUserDto): Promise<void> {
    const { username, email, password, roleId } = registerUserDto;
    const hashedPassword = await this.encoderService.encodePassword(password);
    return this.usersRepository.createUser(username, email, hashedPassword, roleId);
  }

  async login(loginDto: LoginDto): Promise<Object> {
    const { email, password } = loginDto;
    const user = await this.usersRepository.findOneByEmail(email);

    if (
      user && (await this.encoderService.checkPassword(password, user.password))
    ) {
      const payload: JwtPayload = { id: user.id, email, active: user.active };
      const accessToken = await this.jwtService.sign(payload);
      const { username, roleId, id } = user;
      return { access_token: accessToken, userData: {username, role_id: roleId, id }};
    }
    throw new UnauthorizedException('Please check your credentials');
  }


  async getUser(userPayloadDto: UserPayloadDto): Promise<User> {
    // const { name } = userPayloadDto;
    return this.usersRepository.findByPayload(userPayloadDto);
  }
}
