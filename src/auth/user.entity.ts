import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';

import { Reservation } from 'src/reservation/reservation.entity'; 

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 20 })
  username: string;

  @Column({ length: 100, unique: true })
  email: string;

  //@Exclude()
  @Column()
  // @Exclude()
  password: string;

  @Column({ type: 'boolean', default: false })
  active: boolean;

  @Column({default: false})
  roleId: number;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;
  
}
