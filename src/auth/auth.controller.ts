import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { UserPayloadDto } from './dto/user-payload.dto';
import { RegisterUserDto } from './dto/register-user.dto';

@Controller('api/auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/register')
  register(@Body() registerUserDto: RegisterUserDto): Promise<void> {
    return this.authService.registerUser(registerUserDto);
  }

  @Post('/login') 
  login(@Body() loginDto: LoginDto): Promise<Object> {
    return this.authService.login(loginDto);
  }

  @Post('/getUser') 
  getUser(@Body() userPayloadDto: UserPayloadDto): Promise<Object> {
    return this.authService.getUser(userPayloadDto);
  }
}
