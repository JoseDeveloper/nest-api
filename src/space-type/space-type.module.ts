import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SpaceTypeController } from './space-type.controller';
import { SpaceTypeService } from './space-type.service';
import { SpaceTypeRepository } from './space-type.repository';


@Module({
  imports: [TypeOrmModule.forFeature([SpaceTypeRepository])],
  controllers: [SpaceTypeController],
  providers: [SpaceTypeService]
})
export class SpaceTypeModule {}
