import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { SpaceType } from './space-type.entity';

@EntityRepository(SpaceType)
export class SpaceTypeRepository extends Repository<SpaceType> {

  async findAll(): Promise<SpaceType[]> {
    return await this.findAll();
  }
}
