import { Controller, Get } from '@nestjs/common';
import { SpaceTypeService } from './space-type.service';

@Controller('api/space-type')
export class SpaceTypeController {
    constructor(private spaceTypeService: SpaceTypeService) { }

    @Get('/')
    getAll(): Promise<Object[]> {

        return this.spaceTypeService.getAll();
    }
}
