import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';

// import { SpaceType } from 'src/space-type/space-type.entity'; 

@Entity()
export class SpaceType {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  name: string;

}
