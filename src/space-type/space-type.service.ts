import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SpaceTypeRepository } from './space-type.repository';

@Injectable()
export class SpaceTypeService {
    constructor(
        @InjectRepository(SpaceTypeRepository) private spaceTypeRepository: SpaceTypeRepository
      ) {}
    
    async getAll(): Promise<Object[]>{
        
        return this.spaceTypeRepository.find();
    }
}
