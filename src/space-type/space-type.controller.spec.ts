import { Test, TestingModule } from '@nestjs/testing';
import { SpaceTypeController } from './space-type.controller';

describe('SpaceTypeController', () => {
  let controller: SpaceTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SpaceTypeController],
    }).compile();

    controller = module.get<SpaceTypeController>(SpaceTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
