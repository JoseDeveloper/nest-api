import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { SpaceType } from 'src/space-type/space-type.entity';

export class ReservationDto {
  id: string;

  userId: string;

  //   @IsNotEmpty()
  //   @Length(6, 20)
  title: string;

  //   @IsNotEmpty()
  reservationDate: Date;

  //   @IsNotEmpty()
  start: Date;

  //   @IsNotEmpty()
  end: Date;

  //   @IsNotEmpty()
  reserve: string;

  //   @IsNotEmpty()
  spaceType: string;

  //   @IsNotEmpty()
  eventType: string;

  //   @IsNotEmpty()
  createdOn: Date;

  // @IsNotEmpty()
  updatedOn: Date;

}