import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Reservation } from './reservation.entity';

@EntityRepository(Reservation)
export class ReservationRepository extends Repository<Reservation> {

  async createReserve(
    userId: string,
    title: string,
    reservationDate: Date,
    start: Date,
    end: Date,
    reserve: string,
    spaceType: string,
    eventType: string
  ): Promise<Reservation> {
    const reservation = this.create({ userId, title, reservationDate, start, end, reserve, spaceType, eventType });

    try {
      return await this.save(reservation);
      
    } catch (e) { console.log(e)
      if (e.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Error saving reservation');
      }
      throw new InternalServerErrorException();
    }
  }

  async updateReserve(
    reservationDto: Reservation
  ): Promise<Reservation> {
    const reservation = this.merge(reservationDto);

    try {
      return await this.save(reservation);
      
    } catch (e) { console.log(e)
      if (e.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Error saving reservation');
      }
      throw new InternalServerErrorException();
    }
  }

  async findAll(): Promise<Reservation[]> {
    return await this.find();
  }

  async findByUserId(userId): Promise<Reservation[]> {
    return await this.find({ userId }); 
  }

}
