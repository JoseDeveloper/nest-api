import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { SpaceType } from 'src/space-type/space-type.entity'; 
import { User } from 'src/auth/user.entity'; 

@Entity()
export class Reservation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  userId: string;

  @ManyToOne(() => User, (user) => user.id)
  // @OneToOne(() => User)
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user: User;

  @Column({ length: 20 })
  title: string;

  @CreateDateColumn()
  reservationDate: Date;

  @CreateDateColumn()
  start: Date;

  @CreateDateColumn()
  end: Date;

  @Column({ length: 20 })
  reserve: string;

  @Column({ length: 20 })
  spaceType: string;

  @Column({ length: 20 })
  eventType: string; 

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

}
