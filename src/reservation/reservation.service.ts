import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReservationRepository } from './reservation.repository';
import { ReservationDto } from './dto/reservation.dto';
import { Reservation } from './reservation.entity';

@Injectable()
export class ReservationService {
    constructor(
        @InjectRepository(ReservationRepository) private reservationRepository: ReservationRepository
      ) {}
    
    async getAll(): Promise<Object[]>{
        
        return this.reservationRepository.find({relations: ['user']});
    }

    async getByUserId(userId: string): Promise<Object[]>{
        
        return this.reservationRepository.findByUserId(userId);
    }

    async registerReservation(reservationDto: ReservationDto): Promise<Reservation[]> {
        const { userId, title, reservationDate, start, end, reserve, spaceType, eventType } = reservationDto;
        await this.reservationRepository.createReserve(userId, title, reservationDate, start, end, reserve, spaceType, eventType);
        
        return await this.reservationRepository.findByUserId(userId);

    }

    async updateReservation(id, reservationDto: Reservation): Promise<Reservation[]> {
        const { userId } = reservationDto;
        await this.reservationRepository.updateReserve(reservationDto);
        
        return await this.reservationRepository.findByUserId(userId);

    }

    async delete(id): Promise<Reservation[]> {
        await this.reservationRepository.delete(id);
        
        return this.reservationRepository.find({relations: ['user']});

    }

    
      
}
