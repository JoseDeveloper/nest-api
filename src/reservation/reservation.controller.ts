import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ReservationDto } from './dto/reservation.dto';
import { ReservationService } from './reservation.service';
import { Reservation } from './reservation.entity';

@Controller('api/reservation')
export class ReservationController {
    constructor(private reservationService: ReservationService) { }

    @Get('/')
    getAll(): Promise<Object[]> {

        return this.reservationService.getAll();
    }

    @Get(':userId')
    getByUserId(@Param('userId') userId: string): Promise<Object[]> {

        return this.reservationService.getByUserId(userId);
    }

    @Post('/')
    register(@Body() reservationDto: ReservationDto): Promise<Reservation[]> {

        return this.reservationService.registerReservation(reservationDto);
    }

    @Put(':id')
    update(@Param('id') id: number, @Body() reservationDto: Reservation): Promise<Reservation[]> {

        return this.reservationService.updateReservation(id, reservationDto);
    }

    @Delete(':id')
    delete(@Param('id') id: number): Promise<Reservation[]> {

        return this.reservationService.delete(id);
    }
}
