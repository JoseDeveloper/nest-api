import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ReservationModule } from './reservation/reservation.module';
import { SpaceTypeModule } from './space-type/space-type.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'hotel',
      entities: ["dist/**/*.entity{.ts,.js}"],
      synchronize: false,
    }),
    AuthModule,
    ReservationModule,
    SpaceTypeModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
